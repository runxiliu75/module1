const SpecialPageContent = ({ currentPage }) => {
  // Render the content for the special page (e.g., based on the clicked image)
  return <div>Special Page {currentPage} Content</div>;
};

export default SpecialPageContent;
