import styles from "./Page7Content.module.css";

import birdImage from "../../image/bird.png";
import CloudBubble from "../Components/ui/CloudBubble.jsx";
export default function Page7Content() {
  return (
    <div>
      <div>
        <img src={birdImage} alt="Bird" className={styles.bird} />
      </div>
      <div className={styles.cloudBubble}>
        <CloudBubble
          text={
            "Now use the structure that you have learnt to write a persuasive paragraph of your own. Here are some topic sentences that Pirate Buddy Fred's friends have thought of. Choose one of them and fill in the persuasive writing guide."
          }
          width={300}
          height={170}
        />
      </div>
    </div>
  );
}
