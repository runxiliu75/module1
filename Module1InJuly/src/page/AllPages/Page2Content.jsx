//import React from "react";

import Paragraph from "../Components/ui/Paragraph.jsx";
import Text from "../Components/ui/Text.jsx";
import styles from "./Page2Content.module.css";
export default function Page2Content() {
  return (
    <div>
      <div className={styles.paragraph}>
        <Paragraph
          text={
            "The main goal of persuasive writing is to convince your readers to agree with your opinion or point of view."
          }
          boldPhrases={"main goal"}
          length={9}
        />
      </div>
      <div className={styles.paragraph1}>
        <Paragraph
          text={
            "You might be trying to convince your parents to let you eat more ice cream, or play another video game."
          }
        />
      </div>
      <div className={styles.paragraph2}>
        <Paragraph
          text={
            "Can you think of one example where you might need to persuade someone about your opinion or point of view?"
          }
        />
      </div>
      <div className={styles.text}>
        <Text width={600} height={159} />
      </div>
    </div>
  );
}
