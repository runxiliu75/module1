import Styles from "./CloudBubble.module.css";
export default function CloudBubble({ width, height, text }) {
  return (
    <div>
      <div
        className={Styles.cloudBubble}
        style={{ width: `${width}px`, height: `${height}px` }}
      >
        {text}
      </div>
    </div>
  );
}
