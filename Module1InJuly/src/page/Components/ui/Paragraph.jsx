/* eslint-disable react/prop-types */
import React from "react";
export default function Paragraph({ text, boldPhrases, length }) {
  const renderText = () => {
    const index = text.indexOf(boldPhrases);
    if (index !== -1) {
      return (
        <React.Fragment>
          {text.substring(0, index)}
          <span style={{ fontWeight: "bold" }}>{boldPhrases}</span>
          {text.substring(index + length)}
        </React.Fragment>
      );
    } else {
      return text;
    }
  };

  return <div>{renderText()}</div>;
}
