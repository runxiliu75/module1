import style from "./Whiteboard.module.css";

export default function Whiteboard({
  width,
  height,
  title,
  content1,
  content2,
  content3,
  color,
}) {
  return (
    <div
      className={style.container}
      style={{
        width: `${width}px`,
        height: `${height}px`,
      }}
    >
      <div className={style.header} style={{ color }}>
        <div className={style.title}>
          <span className={style.largeLetters}>{title.substr(0, 4)}</span>
          {title.substr(4)}
        </div>
        <hr className={style.line} />
      </div>
      <div className={style.content}>
        <div className={style.content1} style={{ color }}>
          {content1}
        </div>
        <div className={style.content2}>{content2}</div>
        <div className={style.content3}>{content3}</div>
      </div>
    </div>
  );
}
