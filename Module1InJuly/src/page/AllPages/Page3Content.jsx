// import React from "react";
import styles from "./Page3Content.module.css";
import Paragraph from "../Components/ui/Paragraph.jsx";
import Whiteboard from "../Components/ui/Whiteboard.jsx";
export default function Page3Content() {
  return (
    <div>
      <div className={styles.paragraph}>
        <Paragraph
          text={
            "Here is a basic structure that all persuasive writing follows:"
          }
        />
      </div>
      <div className={styles.container}>
        <div className={styles.yourClaim}>
          <Whiteboard
            width={310}
            height={163}
            title={"1) Your claim or argument"}
            content1={""}
            content2={"For example, you might say:"}
            content3={'"I think that science is the best subject at school."'}
            color={"#F3C67F"}
          />
        </div>
        <div className={styles.supportingDetails}>
          <Whiteboard
            width={338}
            height={185}
            title={"2) Supporting details"}
            content1={"These are the reasons why you believe something."}
            content2={
              "For example, for the previous claim, your reasons might be:"
            }
            content3={
              '"It\'s a very interactive class," and "Our teacher, Mr. Brown, is extremely friendly."'
            }
            color={"#E16C7A"}
          />
        </div>
        <div className={styles.evidence}>
          <Whiteboard
            width={347}
            height={175}
            title={"3) Evidence"}
            content1={
              "These are facts that you can use to support your address."
            }
            content2={"For example:"}
            content3={
              '"We get to do interesting experiments in the lab, and "Mr.Brown is always ready to help whenever we need it."'
            }
            color={"#79589D"}
          />
        </div>
      </div>
    </div>
  );
}
