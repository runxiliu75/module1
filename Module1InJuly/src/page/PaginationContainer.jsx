import { useState } from "react";
import PageContent from "./PageContent.jsx";
import SpecialPageContent from "./SpecialPageContent.jsx";
import PaginationControls from "./PaginationControls.jsx";

const PaginationContainer = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [isSpecialPage, setIsSpecialPage] = useState(false);
  const totalPages = 100;
  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
      setIsSpecialPage(false);
    }
  };

  const handleNextPage = () => {
    // Check if there are more pages
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
      setIsSpecialPage(false);
    }
  };

  const handleImageClick = (page) => {
    setCurrentPage(page);
    setIsSpecialPage(true);
  };

  const renderPageContent = () => {
    if (isSpecialPage) {
      return <SpecialPageContent currentPage={currentPage} />;
    } else {
      return <PageContent currentPage={currentPage} />;
    }
  };

  return (
    <div>
      {renderPageContent()}
      <PaginationControls
        currentPage={currentPage}
        handlePrevPage={handlePrevPage}
        handleNextPage={handleNextPage}
        handleImageClick={handleImageClick}
      />
    </div>
  );
};

export default PaginationContainer;
