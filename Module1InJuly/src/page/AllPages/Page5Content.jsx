import styles from "./Page5Content.module.css";
import birdImage from "../../image/bird.png";
import CloudBubble from "../Components/ui/CloudBubble.jsx";
export default function Page5Content() {
  return (
    <div>
      <div>
        <img src={birdImage} alt="Bird" className={styles.bird} />
      </div>
      <div className={styles.cloudBubble}>
        <CloudBubble
          text={
            "Pirate Buddy Fred also has an opinion! Read what he has to say and then identify his claim, reasons and evidence. I have filled in the main claim and his first reason and evidence for it as an example. Can you find the second reason and evidence?"
          }
          width={300}
          height={170}
        />
      </div>
    </div>
  );
}
