//import React from "react";
import birdImage from "../../image/bird.png";
import Paragraph from "../Components/ui/Paragraph.jsx";
import styles from "./Page1Content.module.css";
export default function Page1Content() {
  return (
    <div>
      <div className={styles.imgAndParagraph}>
        <img src={birdImage} alt="Bird" />
        <div className={styles.paragraph}>
          <Paragraph
            text={
              "Hello, kids! Today, we are going to start a different kind of writing - persuasive writing. To help us navigate the perils of persuasive writing, here is our Pirate Buddy Fred, who will be coming along on our journey!"
            }
            boldPhrases={"persuasive writing"}
            length={18}
          />
        </div>
      </div>
    </div>
  );
}
