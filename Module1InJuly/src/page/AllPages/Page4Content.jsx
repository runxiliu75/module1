import styles from "./Page4Content.module.css";
import birdImage from "../../image/bird.png";
import CloudBubble from "../Components/ui/CloudBubble.jsx";
export default function Page4Content() {
  return (
    <div>
      <div>
        <img src={birdImage} alt="Bird" className={styles.bird} />
      </div>
      <div className={styles.cloudBubble}>
        <CloudBubble
          text={
            "But wait! How exactly do you persuade someone to believe your opinion or point of view?"
          }
          width={200}
          height={100}
        />
      </div>
    </div>
  );
}
