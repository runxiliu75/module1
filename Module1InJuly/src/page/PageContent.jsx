import Page1Content from "./AllPages/Page1Content.jsx";
import Page2Content from "./AllPages/Page2Content.jsx";
import Page3Content from "./AllPages/Page3Content.jsx";
import Page4Content from "./AllPages/Page4Content.jsx";
import Page5Content from "./AllPages/Page5Content.jsx";
import Page6Content from "./AllPages/Page6Content.jsx";
import Page7Content from "./AllPages/Page7Content.jsx";
// import Page8Content from "./AllPages/Page8Content.jsx";
// import Page9Content from "./AllPages/Page9Content.jsx";
// import Page10Content from "./AllPages/Page10Content.jsx";
// import Page11Content from "./AllPages/Page11Content.jsx";
// import Page12Content from "./AllPages/Page12Content.jsx";
// import Page13Content from "./AllPages/Page13Content.jsx";
// import Page14Content from "./AllPages/Page14Content.jsx";
// import Page15Content from "./AllPages/Page15Content.jsx";
// import Page16Content from "./AllPages/Page16Content.jsx";
// import Page17Content from "./AllPages/Page17Content.jsx";
// import Page18Content from "./AllPages/Page18Content.jsx";
// import Page19Content from "./AllPages/Page19Content.jsx";
// import Page20Content from "./AllPages/Page20Content.jsx";

export default function PageContent({ currentPage }) {
  const pageComponents = {
    1: Page1Content,
    2: Page2Content,
    3: Page3Content,
    4: Page4Content,
    5: Page5Content,
    6: Page6Content,
    7: Page7Content,
    // 8: Page8Content,
    // 9: Page9Content,
    // 10: Page10Content,
    // 11: Page11Content,
    // 12: Page12Content,
    // 13: Page13Content,
    // 14: Page14Content,
    // 15: Page15Content,
    // 16: Page16Content,
    // 17: Page17Content,
    // 18: Page18Content,
    // 19: Page19Content,
    // 20: Page20Content,
  };

  const PageComponent = pageComponents[currentPage];

  return <div>{PageComponent && <PageComponent />}</div>;
}
