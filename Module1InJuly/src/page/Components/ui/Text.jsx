import { useState } from "react";
import styles from "./Text.module.css";

export default function Text({ width, height }) {
  const [content, setContent] = useState("");

  const handleInputChange = (event) => {
    setContent(event.target.value);
  };

  const containerStyle = {
    width: width,
    height: height,
  };

  const contentStyle = {
    width: `${width}px`,
    height: `calc(${height} - 75px)`,
  };

  return (
    <div className={styles.textContainer} style={containerStyle}>
      <textarea
        className={styles.textContent}
        style={contentStyle}
        value={content}
        onChange={handleInputChange}
        placeholder="Write your notes here..."
      />
    </div>
  );
}
