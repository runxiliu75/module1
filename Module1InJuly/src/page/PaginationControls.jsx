// PaginationControls.jsx
import styles from "./PaginationControls.module.css";
const PaginationControls = ({
  currentPage,
  handlePrevPage,
  handleNextPage,
  // handleImageClick,
}) => {
  // const handleImagePageClick = (page) => {
  //   handleImageClick(page);
  // };
  console.log(currentPage);
  const totalPages = 200;
  const isSpecialPage = [8].includes(currentPage);
  const shouldShowPrevButton = currentPage > 1 && !isSpecialPage;
  const shouldShowNextButton = currentPage < totalPages && !isSpecialPage;

  return (
    <div>
      {shouldShowPrevButton && (
        <button className={styles.btnBack} onClick={handlePrevPage}>
          Back
        </button>
      )}
      {shouldShowNextButton && (
        <button className={styles.btnNext} onClick={handleNextPage}>
          Next
        </button>
      )}
      {/* <button onClick={() => handleImagePageClick(6)}>Go to Page 6</button>
      <button onClick={() => handleImagePageClick(5)}>Go to Page 5</button>
      <button onClick={() => handleImagePageClick(4)}>Go to Page 4</button> */}
    </div>
  );
};

export default PaginationControls;
