import PaginationContainer from "./page/PaginationContainer.jsx";
import Comments from "./comments/Comments.jsx";
import "./App.css";

function App() {
  return (
    <>
      <div className="container">
        <PaginationContainer />
        <div className="comment-container">
          <Comments currentUserId="1" />
        </div>
      </div>
    </>
  );
}

export default App;
